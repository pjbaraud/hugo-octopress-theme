
function makeAsLink(image) {
	linkNode = document.createElement('a');
	linkNode.setAttribute('href',image.src);
	linkNode.setAttribute('target','_blank');
	parentNode = image.parentNode;
	parentNode.removeChild(image);
	parentNode.append(linkNode);
	linkNode.append(image);
}

let articleImages = document.querySelectorAll('article img');
articleImages.forEach( image => makeAsLink(image));
